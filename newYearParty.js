process.stdin.resume();
process.stdin.setEncoding('ascii');

let input_stdin = "";
let input_stdin_array = "";

process.stdin.on('data', function (data) {
  input_stdin += data;
});

process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split("\n");
  let [T, ...input] = input_stdin_array;
  if (( T < 1 || T > 10 )) {
    return exit('')
  }

  const output = recursiveChecker(T, input);

  return exit(output);
});

function recursiveChecker(T, input) {
  if (T <= 0) return '';

  let [N, houses, ...others] = input;
  N = parseInt(N);
  if (N < 1 || N > 1000) return '';

  T--;

  houses = getNumericArray(houses);

  if (houses.length === N) {
    let output = calculateWinner(houses, N) + '\n';
    return output + recursiveChecker(T, others);
  }
}

function calculateWinner(houses, N) {
  // {[index] : {maxSum : NUM, elements : []}}
  let obj = {};

  houses.forEach((house, index) => obj[index] = { maxSum: house, elements: [house] });

  for (let currentIndex = N - 1; currentIndex >= 0; currentIndex--) {
    let valueAtCurrentIndex = houses[currentIndex];

    for (let index = currentIndex + 2; index < N; index++) {
      let { maxSum: maxValueAtNextIndex, elements: elementsAtNextIndex } = obj[index];
      let newElements = elementsAtNextIndex;
      let { maxSum: oldMaxSum, elements: oldElements } = obj[currentIndex];

      if (maxValueAtNextIndex + valueAtCurrentIndex >= oldMaxSum) {
        if (maxValueAtNextIndex + valueAtCurrentIndex === oldMaxSum) {
          newElements = getNewElementsArray(elementsAtNextIndex, oldElements);
          if (newElements === oldElements) {
            newElements = oldElements;
          } else {
            newElements = [valueAtCurrentIndex, ...newElements];
          }
        } else {
          newElements = [valueAtCurrentIndex, ...elementsAtNextIndex]
        }
        obj[currentIndex] = { maxSum: maxValueAtNextIndex + valueAtCurrentIndex, elements: newElements };
      }
    }
  }
  // calculate final result
  let winnerMaxSum = -Infinity, winnerElements = [];
  Object.keys(obj).forEach(key => {
    const { elements, maxSum } = obj[key];

    if (maxSum >= winnerMaxSum) {
      if (winnerMaxSum === maxSum) {
        winnerElements = getNewElementsArray(elements, winnerElements);
      } else {
        winnerElements = elements;
      }
      winnerMaxSum = maxSum;
    }
  });

  let result = '';
  winnerElements.reverse().forEach(element => {
    result = `${result}${element}`
  });
  return result;
}

function getNewElementsArray(newElements, oldElements) {
  let newI = newElements.length - 1;
  let oldI = oldElements.length - 1;
  while (newI >= 0 && oldI >= 0) {
    if (newElements[newI] > oldElements[oldI]) {
      return newElements;
    } else if (newElements[newI] < oldElements[oldI]) {
      return oldElements;
    }
    newI--;
    oldI--;
  }
  return newElements;
}

function exit(output) {
  return process.stdout.write(output);
}

function getNumericArray(arr) {
  return arr.split(' ').filter(a => !isNaN(parseFloat(a))).map(a => parseFloat(a));
}