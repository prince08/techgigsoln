process.stdin.resume();
process.stdin.setEncoding('ascii');

let input_stdin = "";
let input_stdin_array = "";

process.stdin.on('data', function (data) {
  input_stdin += data;
});

process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split("\n");
  let [T, ...input] = input_stdin_array;
  if (( T < 1 || T > 5 )) {
    return exit('')
  }

  const output = recursiveChecker(T, input);

  return exit(output);
});

function recursiveChecker(T, input) {
  if (T <= 0) return '';

  let [N, boxes, ...others] = input;
  N = parseInt(N);
  if (N < 1 || N > 100) return '';

  T--;

  boxes = getNumericArrayFilteredArray(boxes);

  if (boxes.length === N) {
    let output = calculateSum(boxes) + '\n';
    return output + recursiveChecker(T, others);
  }
}

function calculateSum(boxes) {
  for (let i = 0; i < boxes.length; i++) {
    if (boxes[i] < 1 || boxes[i] > 100000) return '';
  }

  boxes = getSortedArray(boxes);
  let sum = 0;
  let map = new Map();

  boxes.forEach(w => {
    if (!isExits(w, map)) {
      sum += w;
      enterInMap(w, map);
    }
  });

  return sum;
}

function exit(output) {
  return process.stdout.write(output);
}

function getNumericArrayFilteredArray(arr) {
  return arr.split(' ').filter(a => !isNaN(parseFloat(a))).map(a => parseFloat(a));
}

function getSortedArray(arr) {
  return arr.sort((a, b) => a > b ? -1 : 1);
}

function enterInMap(num, map) {
  while (num) {
    map.set(( num % 10 ), true);
    num = parseInt(num / 10, 10);
  }
}

function isExits(num, map) {
  while (num) {
    if (map.has(num % 10)) {
      return true
    }
    num = parseInt(num / 10, 10);
  }
  return false;
}