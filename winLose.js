process.stdin.resume();
process.stdin.setEncoding('ascii');

let input_stdin = "";
let input_stdin_array = "";
let input_currentline = 0;

process.stdin.on('data', function (data) {
  input_stdin += data;
});

process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split("\n");
  let [T, ...input] = input_stdin_array;
  if (( T < 1 || T > 10 )) {
    return exit('')
  }

  const output = recursiveChecker(T, input);

  return exit(output);
});

function recursiveChecker(T, input) {
  if (T <= 0) return '';

  let [N, vilianStrength, heroEnergy, ...others] = input;
  N = parseInt(N);
  if (N < 1 || N > 1000) return '';

  T--;

  vilianStrength = getNumericArray(vilianStrength);
  heroEnergy = getNumericArray(heroEnergy);

  if (vilianStrength.length === heroEnergy.length && heroEnergy.length === N) {
    let output = calculateWinner(vilianStrength, heroEnergy, N) + '\n';
    return output + recursiveChecker(T, others);
  }
}

function calculateWinner(vilianStrength, heroEnergy, N) {
  vilianStrength = getSortedArray(vilianStrength);
  heroEnergy = getSortedArray(heroEnergy);
  for (let i = 0; i < N; i++) {
    if (heroEnergy[i] < vilianStrength[i]) {
      return "LOSE"
    }
  }
  return "WIN";
}

function exit(output) {
  return process.stdout.write(output);
}

function getNumericArray(arr) {
  return arr.split(' ').filter(a => !isNaN(parseFloat(a))).map(a => parseFloat(a));
}

function getSortedArray(arr) {
  return arr.sort((a, b) => a < b ? -1 : 1);
}