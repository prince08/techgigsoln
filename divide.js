function divide(dividend, divisor) {
  const sign = dividend * divisor < 0 ? -1 : 1;
  if(divisor == 0){
    return new Error("Division by 0 is not possible.");
  }
  dividend = Math.abs(dividend);
  divisor = Math.abs(divisor);
  let quotient = 0;

  while (dividend > divisor) {
    dividend -= divisor;
    quotient++;
  }
  return quotient * sign;
}

console.log(divide(22, 7));
console.log(divide(-22, -7));
