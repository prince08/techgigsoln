process.stdin.resume();
process.stdin.setEncoding('ascii');

var input_stdin = "";
var input_stdin_array = "";
var input_currentline = 0;

process.stdin.on('data', function (data) {
  input_stdin += data;
});


process.stdin.on('end', function () {
  input_stdin_array = input_stdin.split("\n");
  
  const output = processArray(input_stdin_array, 0, 1);
  
  process.stdout.write(""+output+"\n");
});


processArray = (array, count, index) => {
  if(array.length < index){
    return count;
  }
  if(array[index] !== '1'){
    return count;
  }
  return processArray(array, count+1, index+1);
};